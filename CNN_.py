import torch.nn as nn

# Определение архитектуры нейросети
class ConvNet(nn.Module):
    def __init__(self, num_classes=3):
        super(ConvNet, self).__init__()
        # первый сверточный слой с ReLU активацией и maxpooling-ом
        #входной кнал 3 (RGB изображение), 64 фильтра с ядром размером 3*3)
        self.conv1 = nn.Conv2d(3, 64, kernel_size=3, stride=1, padding=1)
        #relu используется после каждого сверточного слоя для введения нелинейност
        self.relu = nn.ReLU()
        #pool применяется после каждого сверточного слоя с ядром размером 2*2 и шагом 2, что уменьшает размер изображения  в 2 раза
        self.pool = nn.MaxPool2d(kernel_size=2, stride=2)
        # второй сверточный слой 
        self.conv2 = nn.Conv2d(64, 128, kernel_size=3, stride=1, padding=1)
        # классификационный слой (принимает вход размером 128*32*32 (результат последнего сверточного сло)
        # и выдаетвыход размером num_classes
        self.fc1 = nn.Linear(128 * 32 * 32, num_classes)

    def forward(self, x):
        x = self.conv1(x)
        x = self.relu(x)
        x = self.pool(x)
        x = self.conv2(x)
        x = self.relu(x)
        x = self.pool(x)
        #изменяется размерность выхода последнего сверточного слоя
        x = x.view(x.size(0), -1)
        x = self.fc1(x)
        return x